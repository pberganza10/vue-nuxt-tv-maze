import { describe, test, expect } from "vitest";
import { render, screen, waitFor } from "@testing-library/vue";
import UiDialog from "./UiDialog.vue";
import userEvent from "@testing-library/user-event";
import { defineComponent, ref } from "vue";

const html = String.raw;

const Container = defineComponent({
  components: { UiDialog },
  template: html`
    <div>
      <button @click="dialog?.open">Open</button>
      <ui-dialog data-testid="dialog" ref="dialog">
        Dialog content
        <button @click="dialog?.close">Close</button>
      </ui-dialog>
    </div>
  `,
  setup() {
    const dialog = ref<InstanceType<typeof UiDialog>>();
    return { dialog };
  },
});

/**
 * Tests are a little bit finicky since no testing environment properly
 * * supports the `dialog` element yet.
 */
describe("UiDialog", () => {
  test("should render", () => {
    render(UiDialog, {
      slots: {
        default: "Dialog content",
      },
    });

    expect(screen.getByText("Dialog content")).toBeInTheDocument();
  });

  test("should start inert", () => {
    const { container } = render(UiDialog, {
      slots: {
        default: "Dialog content",
      },
    });

    expect(container.firstChild).toHaveAttribute("inert");
  });

  test("should not be inert when open", async () => {
    const user = userEvent.setup();
    render(Container);

    expect(screen.getByTestId("dialog")).toHaveAttribute("inert");

    await user.click(screen.getByRole("button", { name: "Open" }));

    expect(screen.getByTestId("dialog")).not.toHaveAttribute("inert");

    await user.click(
      screen.getByRole("button", { name: "Close", hidden: true })
    );

    expect(screen.getByTestId("dialog")).toHaveAttribute("inert");
  });

  test("should lock scroll when open", async () => {
    const user = userEvent.setup();
    render(Container);

    expect(document.body).not.toHaveStyle("overflow: hidden");

    await user.click(screen.getByRole("button", { name: "Open" }));

    expect(document.body).toHaveStyle("overflow: hidden");

    await user.click(
      screen.getByRole("button", { name: "Close", hidden: true })
    );

    expect(document.body).not.toHaveStyle("overflow: hidden");
  });
});
