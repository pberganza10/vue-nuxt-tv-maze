import { test, expect } from "@playwright/test";

test("X closes the drawer", async ({ page }) => {
  await page.goto("/");
  const genresDialog = page.locator("#side-drawer");
  await expect(genresDialog).toHaveAttribute("inert", "");
  await page.getByRole("button", { name: "Menu" }).click();
  await expect(genresDialog).not.toHaveAttribute("inert", "");
  await page.getByRole("button", { name: "Close" }).click();
  await expect(genresDialog).toHaveAttribute("inert", "");
});

test('should scroll to "Action" genre', async ({ page }) => {
  await page.goto("/");
  await page.getByRole("button", { name: "Menu" }).click();
  await page.getByRole("link", { name: "Action" }).click();
  await expect(page).toHaveURL("/#Action-genre");
});
