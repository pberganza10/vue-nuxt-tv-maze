import { test, expect } from "@playwright/test";

test("navigates to show detail", async ({ page }) => {
  await page.goto("/");
  const link = page.locator("a", { hasText: /under the Dome/i }).first();
  await expect(link).toBeVisible();
  await link.click();
  await expect(
    page.getByRole("heading", { name: /under the dome/i })
  ).toBeVisible();
});
