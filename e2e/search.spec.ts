import { test, expect } from "@playwright/test";

test('should search for "Scream Queens"', async ({ page }) => {
  await page.goto("/");

  await page.getByRole("button", { name: "Search" }).first().click();
  await page.getByPlaceholder("Search").fill("Scream Queens");
  await page
    .locator("button")
    .filter({ hasText: /^Search$/ })
    .click();
  await expect(
    page.getByRole("link", { name: "Scream Queens", exact: true }).first()
  ).toBeVisible();
});

test("should clear input on close", async ({ page }) => {
  await page.goto("/");

  await page.getByRole("button", { name: "Search" }).first().click();
  await page.getByPlaceholder("Search").fill("Scream Queens");
  await page.keyboard.press("Escape");
  await page.getByRole("button", { name: "Search" }).first().click();
  await expect(page.getByPlaceholder("Search")).toHaveValue("");
});
