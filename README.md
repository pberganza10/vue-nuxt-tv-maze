# TV Maze by Genre

This repo contains an app that shows the first few pages of TV Maze's API and categorizes them by genre.
It also allows you to see more details of a specific show and search for them.

## Stack

The stack chosen to make this was:

- [Vue 3](https://vuejs.org): The main UI framework
- [Nuxt](https://nuxt.com): Chosen since it provides a minimal starter template.
- [Shoelace](https://shoelace.style): Framework agnostic components. Picked
  a few components from it since it provides nice and accessible defaults.
- [VueUse](https://vueuse.org): Collection of utilities for VueJS.
- [@testing-library/vue](https://testing-library.com/docs/vue-testing-library/intro): Utilities for unit tests
- [vitest](https://vitest.dev): Test runner based on Vite, compatible with Jest
- [Playwright](https://playwright.dev): End-to-end test runner.

### Nuxt

Nuxt was chosen mainly to have a file based router by default. Some other features
of it, such as auto-imports and data fetching utilities, were a welcome addition.

### Shoelace

Shoelace is a web component library that implements common UI elements in an
accessible way. For this project, the `tooltip`, `spinner`, `rating` and `format-date` elements are used.

Originally brought in order to use its carousel, modal dialog and side drawer,
but the native dialog element was sufficient for both the drawer and modal;
and a horizontal scroll with snapping plus some keyboard interactions was sufficient and more performant for the horizontal scroll.

### VueUse

VueUse is a collection of composables and directives for Vue. Used it in order to handle the body
scroll lock when a dialog is open, to handle element resizing (instead of a `ResizeObserver`),
to handle visibility of the list (instead of an `IntersectionObserver`), and to watch the window
scroll in order to hide or show the "Scroll to top" button.

### @testing-library/vue

Vue Testing Library is a library that provides utilities for testing UI components during unit tests.
The main reason for choosing it is that it promotes testing _behaviour_ rather than _implementation_.

### vitest

Test runner chosen mainly because Nuxt already uses Vite under the hood, so it uses the same
build tool for testing and running.

### Playwright

Unit tests on the front-end can only get you so far. There comes a moment when you might be testing
the DOM implementation on NodeJS rather than the behaviour of your UI elements. (E.g., the `<dialog>`
element is not propertly supported in `happy-dom` and not implemented at all in `jsdom`).

For these scenarios, running tests on a _real_ browser is a must. Playwright provides a testing
API that resembles `@testing-library/vue`, while allowing you to run your tests in mupltiple browser
engines.

## Build dependencies

If you use [Volta](https://volta.sh), the Node version used to run and build this is already pinned.

- NodeJS version: 18.17.1
- pnpm version: 8.7.0

## Accessibility considerations

### Horizontal scroll container

In order not to make users that use tab-navigation need to go through the _whole_
list of shows before getting to the next section, the horizontal container works
as a "listbox", where only the scroll container itself is focusable, but the user can select an item using the arrow keys.

Supported keys:

- Enter: opens the current selected show's detail page.
- Left or Up arrow key: selects the previous show on the list (or loops to the last on the list).
- Right or Down arrow key: selects the next show on the list (or loops back to the beginning).
- Home: selects the first show on the list
- End: selects the last show on the list

### Search dialog and side drawer

The search dialog and side drawer use the native `<dialog>` element which lacks
a few features users are accustomed to. For instance: clicking on the overlay to
close the dialog, locking the scroll of the body when the dialog is open, and animations
when opening/closing it. These have been added.

The dialogs respect `(prefers-reduced-motion)` by only transitioning opacity if the setting is enabled.

The search dialog can be opened with the following keyboard shortcuts from anywhere
on the page: `Cmd + K`, `Ctrl + K` or just `\`.

In order to be able to animate the dialog, some accessibility considerations had to be
established. In order to be able to animate the dialog the default behaviour should be changed by:

- Preventing the dialog from being hidden using `display: none`.
- Manually adding and removing the `inert` attribute from the dialog so it's not reachable by keyboard users.

## Performance considerations

### Initial home page load

The API call to get the list of shows is quite slow, this caused the first contentful
paint to be _very_ slow if the request was done in the server. In order to make the UI feel
a bit more "snappy", this specific request is done client-only, and a spinner is shown.

The "Performance" score in Lighthouse is still not great, but it _feels_ faster than a
server rendered page in this scenario.

## Setup

Make sure to install the dependencies:

```bash
pnpm install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
pnpm dev
```

## Production

Build the application for production:

```bash
pnpm build
```

> NOTE: If any errors occur during a production build,
> you may need to clean-up your build files: `pnpm dlx nuxi@latest cleanup`

Locally preview production build:

```bash
pnpm preview
```

## Unit tests

There's some unit tests available for some reusable functions and the dialog
component. To run them, use the `test` command:

```bash
pnpm test
```

## E2E tests

There's some E2E tests available with Playwright, run them with the e2e command in order
build and preview a production server beforehand:

```bash
pnpm run e2e
```

If you're getting any errors when building/running e2e tests, you'll most likely need to
cleanup the build files and try again.

```bash
pnpm dlx nuxi@latest cleanup
pnpm run e2e
```
