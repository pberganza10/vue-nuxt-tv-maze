// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  vue: {
    compilerOptions: {
      isCustomElement: (tag) => tag.startsWith("sl-"),
    },
  },
  devtools: { enabled: true },
  ssr: true,
  app: {
    pageTransition: {
      name: "page",
      mode: "out-in",
    },
    head: {
      meta: [
        {
          name: "description",
          content: "Look at TV Maze's database by genre",
        },
      ],
    },
  },
  css: [
    "~/assets/reset.css",
    "~/assets/fonts.css",
    "@shoelace-style/shoelace/dist/themes/light.css",
    "~/assets/main.css",
  ],
});
