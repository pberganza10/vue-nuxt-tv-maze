export default defineNuxtPlugin(async () => {
  await import("@shoelace-style/shoelace/dist/components/tooltip/tooltip.js");
  await import("@shoelace-style/shoelace/dist/components/rating/rating.js");
  await import(
    "@shoelace-style/shoelace/dist/components/format-date/format-date.js"
  );
  await import("@shoelace-style/shoelace/dist/components/spinner/spinner.js");
});
