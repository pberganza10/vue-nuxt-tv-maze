import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import autoImport from "unplugin-auto-import/vite";
import vueComponents from "unplugin-vue-components/vite";

export default defineConfig({
  plugins: [
    vue(),
    autoImport({
      imports: ["vue", "vue-router"],
      dirs: ["composables/**", "utils/**"],
    }),
    vueComponents({
      dirs: ["components/**"],
      directoryAsNamespace: true,
    }),
  ],
  test: {
    globals: true,
    environment: "happy-dom",
    setupFiles: ["./setup.vitest.ts"],
    exclude: ["e2e/**", "node_modules/**"],
  },
});
