import { describe, test, expect } from "vitest";
import { waitFor } from "@testing-library/vue";
import useBodyScrollLock from "./useBodyScrollLock";

describe("useBodyScrollLock", () => {
  test("should lock scroll", async () => {
    const locked = useBodyScrollLock();
    expect(locked.value).toBe(false);
    expect(document.body.style.overflow).toBe("");
    locked.value = true;
    await waitFor(() => {
      expect(document.body.style.overflow).toBe("hidden");
    });
  });
});
