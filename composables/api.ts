import type { AsyncDataOptions } from "nuxt/app";

const PAGES_TO_FETCH = 2;

export type Show = {
  id: number;
  url: string;
  name: string;
  type: string;
  language: string;
  genres: string[];
  status: string;
  runtime: number;
  averageRuntime: number;
  premiered?: string;
  ended?: string;
  officialSite: string;
  rating: {
    average: number;
  };
  weight: number;
  network: {
    id: number;
    name: string;
    country: {
      name: string;
      code: string;
      timezone: string;
    };
    officialSite: string;
  };
  externals: {
    tvrage: number;
    thetvdb: number;
    imdb: string;
  };
  image: {
    medium: string;
    original: string;
  };
  summary: string;
};

export type Genres = Record<string, Show[]>;

const baseURL = "https://api.tvmaze.com";

async function getShowsByPage(page: MaybeRefOrGetter<number>) {
  const cache = useState<Show[] | null>(`shows-page-${toValue(page)}`);
  if (cache.value) return cache.value;
  const pages = Array.from({ length: PAGES_TO_FETCH }, (_, i) =>
    $fetch<Show[]>(`/shows?page=${toValue(page) + i}`, { baseURL })
  );
  cache.value = (await Promise.all(pages))
    .flat()
    .sort((a, b) => b.rating.average - a.rating.average);
  return cache.value;
}

type SearchResult = {
  score: number;
  show: Show;
};

async function getShowsBySearch(q: MaybeRefOrGetter<string>) {
  const cache = useState<Show[] | null>(`shows-search-${toValue(q)}`);
  if (cache.value) return cache.value;
  const shows = await $fetch<SearchResult[]>(`/search/shows?q=${toValue(q)}`, {
    baseURL,
  });
  cache.value = shows
    .map((show) => show.show)
    .sort((a, b) => b.rating.average - a.rating.average);
  return cache.value;
}

export function useSearch(q: MaybeRefOrGetter<string>) {
  return useLazyAsyncData(() => getShowsBySearch(q), {
    watch: [toRef(q)],
  });
}

export function useShows<DataT = Show[]>(
  page: MaybeRefOrGetter<number>,
  q?: MaybeRefOrGetter<string | undefined>,
  options?: AsyncDataOptions<Show[], DataT>
) {
  return useLazyAsyncData(
    () => {
      const searchQuery = toValue(q);
      if (searchQuery) return getShowsBySearch(searchQuery);
      return getShowsByPage(page);
    },
    {
      ...options,
      server: false,
      watch: [toRef(page), toRef(q), ...(options?.watch ?? [])],
    }
  );
}

function transformByGenre(shows: Show[]) {
  const genres: Genres = {};
  shows?.forEach((show) => {
    show.genres.forEach((genre) => {
      if (!genres[genre]) genres[genre] = [];
      genres[genre].push(show);
    });
  });
  return genres;
}

export function useGenres(
  page: MaybeRefOrGetter<number>,
  q?: MaybeRefOrGetter<string | undefined>
) {
  return useShows(page, q, { transform: transformByGenre });
}

async function getShow(id: MaybeRefOrGetter<number>) {
  const cache = useState<Show | null>(`show-${toValue(id)}`);
  if (cache.value) return cache.value;
  const show = await $fetch<Show>(`/shows/${toValue(id)}`, { baseURL });
  cache.value = show;
  return cache.value;
}

export function useShow(id: MaybeRefOrGetter<number>) {
  return useLazyAsyncData(() => getShow(id), {
    watch: [toRef(id)],
  });
}
