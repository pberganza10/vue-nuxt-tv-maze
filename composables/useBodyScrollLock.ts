import { useScrollLock } from "@vueuse/core";

export default function useBodyScrollLock() {
  if (process.server) {
    return ref(false);
  }
  return useScrollLock(document.body);
}
