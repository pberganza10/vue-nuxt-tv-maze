import type { RouterConfig } from "@nuxt/schema";

// https://router.vuejs.org/api/interfaces/routeroptions.html
export default <RouterConfig>{
  async scrollBehavior(to, from, savedPosition) {
    await new Promise((resolve) => setTimeout(resolve, 500));
    if (to.hash) {
      return {
        el: to.hash,
      };
    } else {
      return savedPosition || { top: 0 };
    }
  },
};
